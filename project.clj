(defproject condense/om-tick "0.1.0-SNAPSHOT"
  :description "Some common code we use at Condense for Om based webapps with forms, fields and state."
  :url "http://bitbucket.com/condense/om-tick"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-3126" :scope "provided"]
                 [figwheel "0.2.5"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [sablono "0.3.4"]
                 [org.omcljs/om "0.8.8"]
                 [org.clojure/core.match "0.3.0-alpha4"]
                 [cljsjs/moment "2.9.0-0"]
                 [cljsjs/pikaday "1.3.2-0"]]

  :plugins [[lein-cljsbuild "1.0.4"]
            [lein-figwheel "0.2.5"]
            [com.cemerick/clojurescript.test "0.3.3"]]

  :source-paths ["src"]

  :clean-targets ^{:protect false} ["resources/public/js/compiled"]

  :cljsbuild {
    :builds [{:id "dev"
              :source-paths ["src" "dev_src" "test"]
              :compiler {:output-to "resources/public/js/compiled/om_tick.js"
                         :output-dir "resources/public/js/compiled/out"
                         :optimizations :none
                         :main om-tick.dev
                         :asset-path "js/compiled/out"
                         :source-map true
                         :source-map-timestamp true
                         :cache-analysis true }}
             {:id "min"
              :source-paths ["src"]
              :compiler {:output-to "resources/public/js/compiled/om_tick.js"
                         :main om-tick.core
                         :optimizations :advanced
                         :pretty-print false}}
             {:id "basics"
              :source-paths ["src" "examples/basics/src"]
              :compiler {:output-to "examples/basics/main.js"
                         :output-dir "examples/basics/out"
                         :source-map true
                         :optimizations :none}}]}

  :figwheel {
             :http-server-root "public" ;; default and assumes "resources"
             :server-port 3450 ;; default+1
             :css-dirs ["resources/public/css"] ;; watch and update CSS

             ;; Start an nREPL server into the running figwheel process
             ;; :nrepl-port 7888

             ;; Server Ring Handler (optional)
             ;; if you want to embed a ring handler into the figwheel http-kit
             ;; server, this is simple ring servers, if this
             ;; doesn't work for you just run your own server :)
             ;; :ring-handler hello_world.server/handler

             ;; To be able to open files in your editor from the heads up display
             ;; you will need to put a script on your path.
             ;; that script will have to take a file path and a line number
             ;; ie. in  ~/bin/myfile-opener
             ;; #! /bin/sh
             ;; emacsclient -n +$2 $1
             ;;
             ;; :open-file-command "myfile-opener"

             ;; if you want to disable the REPL
             ;; :repl false

             ;; to configure a different figwheel logfile path
             ;; :server-logfile "tmp/logs/figwheel-logfile.log"
             })
