(ns om-tick.form-test
  (:require-macros [cemerick.cljs.test
                    :refer (is deftest with-test run-tests testing test-var)])
  (:require
    [cemerick.cljs.test :as t]
    [om-tick.form :refer [load-data initialise-form]]))


(deftest test-load-data
         (let [data1 {:data {:a "asdf"}}
               data2 {:data {:a "asdf" :b true}}]

           (is (= (load-data data1)
                  (assoc data1 :fields {:a {:value "asdf"}})))

           (is (= (load-data data2)
                  (assoc data2 :fields {:a {:value "asdf"}
                                        :b {:value true}})))))


(deftest test-initialise-form
         (let [form1 {:fields {:name {:placeholder "Your name"}
                               :age {:initial "3/11/1975"}
                               :occupation {}}
                      :data {:occupation "Farmer"}}]

           (testing "No affect when not disabled"
                    (is (= (initialise-form form1)
                           (-> form1
                               (assoc-in [:fields :age :value] "3/11/1975")
                               (assoc-in [:fields :name :value] nil)
                               (assoc-in [:fields :occupation :value] "Farmer")))))))


