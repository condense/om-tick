(ns om-tick.app-test
  (:require-macros [cemerick.cljs.test
                    :refer (is deftest with-test run-tests testing test-var)])
  (:require
    [cemerick.cljs.test :as t]
    [om-tick.app :refer [initalise-apps derive-state-apps]]
    [om-tick.app :as app]))


(deftest test-watcher-apps
         (let [iref (atom {:a 1})
               oref (atom 1)
               count-change (reify
                              app/IObserveState
                              (observe-state [_ _ o n]
                                (swap! oref inc)))]
           (add-watch iref [::observe-state]
             (fn [k r o n]
               (app/observe-state-apps [k r o n] [count-change])))
           (testing "Simpler watcher"
                    (swap! iref assoc :a 2)
                    (is (= {:a 2} @iref))
                    (is (= 2 @oref)))))


(deftest test-logic-apps
         (let [inc-counters (reify
                              app/IDeriveState
                              (derive-state [_ state]
                                (update state :counter #(map inc %))))]
           (testing "Can derive state using app"
                    (is (= {:counter [2 4]}
                           (app/derive-state inc-counters {:counter [1 3]}))))
           (testing "Simple derived state app."
                    (is (= {:counter [1 2 3]}
                           (derive-state-apps {:counter [0 1 2]} [inc-counters]))))
           (testing "Simple derived state app, twice."
                    (is (= {:counter [2 3 4]}
                           (derive-state-apps {:counter [0 1 2]} [inc-counters inc-counters]))))))


(deftest test-initalise-apps
         (let [hello (reify
                       app/IInitState
                       (init-state [_ state]
                         (assoc state :hello nil)))
               form-flag (reify
                           app/IAppActive
                           (app-active? [_ state]
                             (contains? state :form))
                           app/IInitState
                           (init-state [_ state]
                             (assoc-in state [:form :flag] true)))]
           (testing "Initialise emptyness"
                    (is (= {}
                           (initalise-apps {} []))))

           (testing "Simple intialise app"
                    (is (= {:hello nil}
                           (initalise-apps {} [hello]))))

           (testing "Optional enabled app initialises"
                    (is (= {:form {:flag true}}
                           (initalise-apps {:form {}}
                                           [form-flag]))))

           (testing "Optional disabled app doesn't initialises"
                    (is (= {}
                           (initalise-apps {}
                                           [form-flag]))))

           (testing "Multiple apps initialises"
                    (is (= {:hello nil :form {:flag true}}
                           (initalise-apps {:form {}}
                                           [hello form-flag]))))))