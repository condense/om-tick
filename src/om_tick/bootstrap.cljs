(ns om-tick.bootstrap
  (:require-macros [cljs.core.async.macros :refer [go-loop alt!]])
  (:require [cljs.core.async :as async :refer [put! <! chan timeout]]
            [cljs.core.match :refer-macros [match]]
            [goog.dom :as gdom]
            [om.core :as om :include-macros true]
            [om.dom :as dom]
            [sablono.core :refer-macros [html]]
            [clojure.string :as string]
            cljsjs.moment
            cljsjs.pikaday.with-moment))


(defn label-template [{:keys [label required]}]
  (if label [:label label (if required " *")]))


(defn help-block-template [{:keys [errors show-errors help]}]
  (if help [:p.help-block help]))


(defn validation-state [{:keys [errors show-errors]}]
  (if (and show-errors (not (empty? errors)))
    "has-error"))


(defn input-template [props owner]
  (let [{:keys [value is-hidden addon-before addon-after]
         :or   {is-hidden false}} props
        input-control [:input.form-control (assoc props
                                             :value (or value "")
                                             :key "ifc")]]
    (if-not is-hidden
      (html [:div.form-group {:class (validation-state props)}
             (label-template props)
             (if (or addon-after addon-before)
               [:div.input-group {:key "ig"}
                addon-before input-control addon-after]
               input-control)
             (help-block-template props)]))))


(defn Input [props owner]
  (om/component
    (input-template props owner)))


(defn Option [props owner]
  (om/component
    (let [[value display] props]
      (dom/option #js {:value value} display))))


(defn select-template [props]
  (let [{:keys [value help disabled errors is-hidden
                options default-option default-value loading]
         :or   {is-hidden false}} props
        disabled (or disabled loading)
        default-value (or default-value "")
        default-option (or default-option "Please select")]
    (assert (coll? options) (str "Expected a collection, got '" options "' " (type options)))
    (if-not is-hidden
      [:div.form-group {:class (validation-state props)}
       (label-template props)
       (vec (concat
              [:select.form-control (assoc props
                                      :value (or value default-value)
                                      :disabled disabled)
               (if options
                 (dom/option #js {:value default-value :disabled true} default-option))]
              (om/build-all Option options)))
       (help-block-template props)])))


(defn Select [props owner]
  (om/component
    (html (select-template props))))


(defn Textarea [props owner]
  (om/component
    (let [{:keys [value]} props]
      (html [:div.form-group {:class (validation-state props)}
             (label-template props)
             [:textarea (assoc props
                          :value (or value "")
                          :class "form-control"
                          :key "textarea")]
             (help-block-template props)]))))


(defn ExpandingTextarea
  "http://alistapart.com/article/expanding-text-areas-made-elegant"
  [props owner]
  (om/component
    (let [{:keys [value]} props]
      (html [:div.form-group {:class (validation-state props)}
             (label-template props)
             [:div.expandingArea.active {:style {:position "relative"}}
              [:pre {:class "form-control"} [:span value] [:br]]
              [:textarea (assoc props
                           :value (or value "")
                           :class "form-control"
                           :key "textarea")]]
             (help-block-template props)]))))


(defn Checkbox [props owner]
  (om/component
    (let [{:keys [label checked on-change disabled]} props
          input-control (dom/input #js {:type     "checkbox"
                                        :checked  checked
                                        :disabled disabled
                                        :onChange on-change})]
      (html [:div.form-group {:class (validation-state props)}
             [:div.checkbox
              [:label input-control label]]
             (help-block-template props)]))))


(defn unpack-date [value]
  (let [m (js/moment value)]
    (if (.isValid m)
      [(.year m) (+ 1 (.month m)) (.date m)])))


(defn just-digits [v]
  (string/replace v #"\D" ""))


(defn WufooDate
  "Date field based on the style used by Wufoo"
  [props owner]
  (reify
    om/IDisplayName (display-name [_] "Date")
    om/IInitState
    (init-state [_] {:event-ch      (chan)
                     :current-state :idle
                     :year          ""
                     :month         ""
                     :day           ""})

    om/IWillMount
    (will-mount [_]
      (let [event-ch (om/get-state owner :event-ch)]
        (go-loop [state :idle]
                 (om/set-state! owner :current-state state)
                 (let [edit! #(let [[y m d] (unpack-date (:value (om/get-props owner)))]
                               (om/set-state! owner :year y)
                               (om/set-state! owner :month m)
                               (om/set-state! owner :day d))

                       blur! #(let [{:keys [year month day]} (om/get-state owner)
                                    value (js/moment (str year "-" month "-" day))
                                    prev-value (js/moment (om/get-props owner :value))]
                               (if (and (.isValid value)
                                        (not= (take 3 (.toArray prev-value))
                                              (take 3 (.toArray value))))
                                 (if-let [on-value-change (om/get-props owner :on-value-change)]
                                   (on-value-change (.format value "YYYY-MM-DD"))))
                               (if-let [on-blur (om/get-props owner :on-blur)]
                                 (on-blur nil)))

                       change! (fn [k v]
                                 (om/set-state! owner k (just-digits v)))]

                   (let [event (<! event-ch)]
                     (recur (match [state event]
                                   [:idle [:focus]] (do (edit!) :edit)
                                   [:edit [:change k v]] (do (change! k v) :edit)
                                   [:edit [:blur]] (do (blur!) :idle)
                                   :else state)))))))

    om/IRenderState
    (render-state [_ {:keys [event-ch current-state year month day]}]
      (let [{:keys [value errors disabled]} props
            [yyyy mm dd] (if (= :idle current-state)
                           (unpack-date value)
                           [year month day])
            event! (fn [& args]
                     (put! event-ch (vec args)) nil)]
        (html [:div.Date {:class    (if (not (empty? errors)) "has-error")
                          :on-blur  #(let [related (.-relatedTarget %)]
                                      (if-not (and related (gdom/contains (.-currentTarget %) related))
                                        (event! :blur)))
                          :on-focus #(event! :focus)}
               (label-template props)
               [:div.DateInputs
                (input-template {:ref        "dd"
                                 :value      (or dd "")
                                 :disabled   disabled
                                 :max-length 2
                                 :on-change  #(event! :change :day (.. % -target -value))
                                 :help       "DD"
                                 :size       2} owner)
                [:span.slash " / "]
                (input-template {:ref        "mm"
                                 :value      (or mm "")
                                 :disabled   disabled
                                 :max-length 2
                                 :on-change  #(event! :change :month (.. % -target -value))
                                 :help       "MM"
                                 :size       2} owner)
                [:span.slash " / "]
                (input-template {:ref        "yyyy"
                                 :value      (or yyyy "")
                                 :disabled   disabled
                                 :max-length 4
                                 :on-change  #(event! :change :year (.. % -target -value))
                                 :help       "YYYY"
                                 :size       4} owner)]
               (help-block-template props)])))))


(defn date-string [m & [format]]
  (if (.isValid m) (.format m (or format "YYYY-MM-DD"))))


(defn normalise-date-string [s & [from to]]
  (-> s (js/moment from) (date-string to)))


(defn PikadayButton
  "Calendar button for use alongside date input controls"
  [props owner]
  (reify

    om/IDisplayName
    (display-name [_] "PikadayButton")

    om/IDidMount
    (did-mount [_]
      (let [picker (new js/Pikaday #js
          {:field    (om/get-node owner "datepicker")
           :trigger  (om/get-node owner "calendar")
           :position "bottom right"
           :onSelect #(if-let [on-pikaday (om/get-props owner :on-pikaday)]
                       (on-pikaday (normalise-date-string %)))})]
        (om/set-state! owner :picker picker)))

    om/IDidUpdate
    (did-update [_ _ _]
      (om/get-props owner :value)
      (.setDate (om/get-state owner :picker) (om/get-props owner :value) true))

    om/IRenderState
    (render-state [_ {:keys [picker]}]
      (let [{:keys []} props]
        (html [:span.input-group-btn
               [:input {:type "hidden" :ref "datepicker"}]
               [:button.btn.btn-default
                {:ref "calendar" :tabIndex "-1"}
                [:span.glyphicon.glyphicon-calendar]]])))))


(defn match-state-to-props
  ([owner k]
   (match-state-to-props owner k identity))
  ([owner k f]
   (om/set-state! owner k (f (om/get-props owner :value)))))


(defn handle-date-change [owner e]
  (let [v (.. e -target -value)]
    (om/set-state! owner :value v)))

(defn handle-date-blur [owner e]
  (let [v (.. e -target -value)]
    (if-let [on-date-change (om/get-props owner :on-date-change)]
      (on-date-change
        (normalise-date-string v
                               (om/get-props owner :display-format)
                               (om/get-props owner :value-format))))
    (if-let [on-blur (om/get-props owner :on-blur)]
      (on-blur e))))


(defn handle-picker-click [owner e]
  (let [picker (om/get-state owner :picker)]
    (.show picker)))


(defn Date [{:keys [display-format value-format is-hidden]
             :or {display-format "YYYY-MM-DD" value-format "YYYY-MM-DD"}
             :as props} owner]
  "
  Date picker field with popup calendar:
   - can type date
   - can pick date from calendar

  Props:
    :label
    :value
    :on-date-change
    :help
    :required
    :errors
    :is-hidden
    :show-errors
    :addon-before
    :addon-after
    :display-format
    :value-format
  "
  (reify

    om/IDisplayName
    (display-name [_] "Date")

    om/IWillMount
    (will-mount [_]
      (match-state-to-props
        owner :value #(normalise-date-string % value-format display-format)))

    om/IDidMount
    (did-mount [_]
      (let [picker (new js/Pikaday #js
          {:field    (om/get-node owner "datepicker")
           :onSelect #(if-let [on-pikaday (om/get-props owner :on-pikaday)]
                       (on-pikaday (normalise-date-string
                                     %
                                     (om/get-props owner :display-format)
                                     (om/get-props owner :value-format))))
           :format   display-format})]
        (om/set-state! owner :picker picker)))

    om/IDidUpdate
    (did-update [_ old-props old-state]
      (let [new-value (om/get-props owner :value)
            old-value (:value old-props)
            prop-value-changed? (not= old-value new-value)]
        (if prop-value-changed?
          (om/set-state! owner :value (normalise-date-string new-value
                                                             value-format
                                                             display-format)))))

    om/IRenderState
    (render-state [_ {:keys [value picker]}]
      (html
        [:div.form-group {:class [(validation-state props)
                                  (if is-hidden "hidden" "")]
                          :style {:width "auto"}}
         (label-template props)
         [:div.form-inline
          [:div.input-group {:key "ig"}
           [:input.form-control (assoc props
                                  :ref "datepicker"
                                  :on-change #(handle-date-change owner %)
                                  :on-blur #(handle-date-blur owner %)
                                  :value value
                                  :key "ifc")]
           [:span.input-group-btn
            [:button.btn {:on-click #(handle-picker-click owner %)}
             [:span.glyphicon.glyphicon-calendar]]]]]
         (help-block-template props)]))))