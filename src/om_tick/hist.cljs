(ns om-tick.hist
  (:require-macros [cljs.core.async.macros :refer [go go-loop alt!]])
  (:require [cljs.core.async :refer [put! <! >! alts! chan pub sub timeout dropping-buffer]]
            [om-tick.app :as app]
            [ajax.core :refer [GET]]))


(defn replaceState!
  "
  Update the current window.history entry with state.
  You can call this often.
  You should call it before using pushState!
  :href defaults to location.href if not provided.
  "
  ([state]
   (println "replacing state")
   (replaceState! state js/location.href))
  ([state href]
   (js/window.history.replaceState
     (clj->js state) "" href)))


(defn pushState!
  "
  Add a new window.history entry with state and href attributes.
  "
  [state href]
  (js/window.history.pushState
    (clj->js state) "" href))


(defn popstate-handler
  "
  Simple popstate handler which resets app-state with the history
  state.  JS state object is first converted to native CLJ objects
  using (js->clj state :keywordize-keys true).

  Where state is not found an AJAX fetch of the URL is performed.
  "
  [{:keys [app-state]} e]

  (if-let [state (.-state e)]
    (reset! app-state (js->clj state :keywordize-keys true))
    (js/console.log "TODO: Fetch state using url")))


(defn get-machine [{:keys [data-ch fail-ch]}
                   {:keys [url params state]}]
  (println :get-machine
           [data-ch fail-ch]
           [url params state])
  (go
    (if state
      (>! data-ch state)
      (GET url {:params          params
                :format          :json
                :response-format :json
                :headers         {"X-Requested-With" "XMLHttpRequest"}
                :keywords?       true
                :handler         #(put! data-ch %)
                :error-handler   #(put! fail-ch %)}))))


(defn goto-machine
  "
  Processes an AJAX based page transition:
  1. Update history entry
  2. Fetch new payload
  3. Initialise payload as state
  4. Reset app-state
  5. Add new history entry

  Assumptions:
  * Same URL for full page load and ajax payload request
  * Full app-state is being updated
  "
  [{:keys [app-state installed-apps]}
   {:keys [news-ch]}
   {:keys [url params state] :as get-args}]
  (let [data-ch (chan)
        fail-ch (chan)
        time-ch (timeout 5000)
        href (if-not params url (str url "?" (ajax.core/params-to-str params)))]
    (go (replaceState! @app-state)
        (>! news-ch :started)
        (<! (get-machine {:data-ch data-ch
                          :fail-ch fail-ch} get-args)))
    (go (alt! data-ch ([data]
                        (>! news-ch [:processing data])
                        (reset! app-state (app/initalise-apps data installed-apps))
                        (pushState! data href)
                        (>! news-ch :success))
              fail-ch (do (>! news-ch :failure))
              time-ch (do (>! news-ch :timed-out))))))


(defn load-machine
  "
  Processes an AJAX based state update:
  1. Update history entry
  2. Fetch new content
  3. Update app-state
  4. Add new history entry

  Assumptions:
  * Same URL for full page load and ajax payload request
  * Full app-state is being updated
  "
  [{:keys [app-state installed-apps]}
   {:keys [news-ch time-ch data-ch fail-ch]
    :or   {time-ch (timeout 5000)}}
   {:keys [url params state swap-fn href] :as get-args}]
  (let [data-ch (chan)
        fail-ch (chan)]
    (go
      (>! news-ch :started)
      (replaceState! @app-state)
      (<! (get-machine {:data-ch data-ch
                        :fail-ch fail-ch} get-args)))
    (go
      (alt! data-ch ([data]
                      (-> (swap! app-state swap-fn data)
                          (pushState! href))
                      (>! news-ch :success))
            fail-ch (do (>! news-ch :failure))
            time-ch (do (>! news-ch :timed-out))))))


(defn news-machine [pub-chan k]
  (let [ch (chan)]
    (go-loop []
             (when-let [msg (<! ch)]
               (js/console.log (pr-str msg))
               (put! pub-chan {:msg msg :topic k})
               (recur)))
    ch))


(defn nav-machine
  "
  Listens on notif-chan for requests and acts on them.

  [:nav :save] will update the current history entry state
  [:nav :goto] will use ajax to fetch new state

  Announces updates via pub-chan.
  "
  [{:keys [notif-chan pub-chan app-state] :as shared} k]
  (let [n (rand)]
    (put! pub-chan {:topic [k :term] :data n})
    (let [term-ch (sub notif-chan [k :term] (chan))
          echo-ch (sub notif-chan [k :echo] (chan))
          save-ch (sub notif-chan [k :save] (chan))
          goto-ch (sub notif-chan [k :goto] (chan))
          load-ch (sub notif-chan [k :load] (chan))
          news-ch (news-machine pub-chan k)]
      (go-loop []
               (>! news-ch {:signal :looping})
               (if (alt!
                     term-ch ([{:keys [data]}] (= n data))
                     echo-ch ([x] (js/console.log "Echo" (pr-str x) n) true)
                     save-ch (do (replaceState! @app-state) true)
                     goto-ch ([goto-args]
                               (>! news-ch [:goto goto-args])
                               (<! (goto-machine shared {:news-ch news-ch} goto-args)))
                     load-ch ([load-args]
                               (<! (load-machine shared {:news-ch news-ch} load-args))))
                 (recur)
                 (>! news-ch {:signal :terminating}))))))


(defn nav-app [shared k]
  (aset js/window "onpopstate" #(popstate-handler shared %))
  (nav-machine shared k)
  (reify))