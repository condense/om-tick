(ns om-tick.field
  (:require-macros [om-tick.debug :refer [inspect]])
  (:require [clojure.zip :as zip :refer [zipper]]
            [cljs.core.match :refer-macros [match]]
            [om.core :as om]))


(defn update-on-change! [field-cursor event]
  (om/update! field-cursor :value (.. event -target -value)))


(defn field? [m]
  (and (map? m)
       (or (contains? m :value)
           (contains? m :type))))


(defn has-errors? [f]
  (not (empty? (:errors f))))


(def empty-values #{nil [] {} #{} ""})


(defn is-unset? [value]
  (contains? empty-values value))


(defn validate-field
  "Add required validation message to empty required values."
  [{:keys [required value] :as field}]
  (inspect
    (match [(field? field) (has-errors? field) required (is-unset? value)]
      [false _ _ _] field
      [_ true _ _] field
      [_ _ true true] (assoc field :errors ["This field is required"])
      :else field)))


(defn reset-field [field]
  (assoc field
    :value (cond
             (contains? field :initial) (:initial field)
             (:many field) []
             :else nil)))


(defn mask-disabled-value [field]
  (if (:disabled field)
    (assoc field :value nil)
    field))


(defn options-to-map [options]
  (into (sorted-map) (map (juxt :Id :Name) options)))


(defn prepare-field-options
  "Field options tend to arrive as lists, normalise to sorted-maps."
  [{:keys [options] :as field}]
  (if (coll? options)
    (assoc field :options (options-to-map options)
                 ::raw-options options)
    field))


(defn initialise-field [field]
  (-> field reset-field prepare-field-options))


(defn branch? [x]
  (cond
    (map? x) (cond (contains? x :value) (:many x)
                   :else true)
    (coll? x) true
    :else false))


(defn children [x]
  (cond
    (map? x) (if (and (contains? x :value) (:many x))
               (map :value (:value x))
               (seq x))
    (coll? x) (seq x)
    :else nil))


(defn make-node [node children]
  (cond
    (map? node) (if (contains? node :value)
                  (assoc node :value children)
                  (into (empty node) children))
    (coll? node) (into (empty node) children)
    :else (into (empty node) children)))


(defn field-zipper [node]
  (zipper branch? children make-node node))


(defn field-reduce
  [zipper f val]
  (loop [loc zipper
         acc val]
    (if (zip/end? loc)
      acc
      (let [node (zip/node loc)]
        (recur (zip/next loc) (if (field? node)
                                (f acc node)
                                acc))))))

(defn field-edit
  ([zipper editor]
    (field-edit zipper (constantly true) editor))
  ([zipper matcher editor]
   (loop [loc zipper]
     (if (zip/end? loc)
       (zip/root loc)
       (if (field? (zip/node loc))
         (if (matcher (zip/node loc))
           (recur (zip/next (zip/edit loc editor)))
           (recur (zip/next loc)))
         (recur (zip/next loc)))))))


(defn tree-edit
  [zipper matcher editor]
  (loop [loc zipper]
    (if (zip/end? loc)
      (zip/root loc)
      (if (matcher (zip/node loc))
        (recur (zip/next (zip/edit loc editor)))
        (recur (zip/next loc))))))
