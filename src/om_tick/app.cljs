(ns om-tick.app)


(defprotocol IInitState
  (init-state [_ state]))


(defprotocol IDeriveState
  (derive-state [_ state]))


(defprotocol IValidateState
  (validate-state [_ state]))


(defprotocol IObserveState
  (observe-state [_ iref o n]))


(defprotocol IAppActive
  (app-active? [_ state]))


(defn -active? [app state]
  (or (not (satisfies? IAppActive app))
      (app-active? app state)))


(defn build-app [shared f k & as]
  (let [app (apply f shared k as)]
    (update shared :installed-apps conj app)))


(defn initalise-apps
  "Initialise apps allowing them to update state"
  [state apps]
  {:pre  [(map? state) (coll? apps)]
   :post [(map? %)]}
  (reduce (fn [state-acc app]
            (if (and (satisfies? IInitState app)
                     (-active? app state-acc))
              (init-state app state-acc)
              state-acc))
          state apps))


(defn derive-state-apps
  "Update state based on apps"
  [state apps]
  {:pre  [(map? state) (coll? apps)]
   :post [(map? %)]}
  (reduce (fn [state-acc app]
            (if (and (satisfies? IDeriveState app)
                     (-active? app state-acc))
              (derive-state app state-acc)
              state-acc))
          state apps))


(defn observe-state-apps
  [[key iref o n] apps]
  {:pre  [(satisfies? IDeref iref) (coll? apps)]}
  (doseq [app apps]
    (if (and (satisfies? IObserveState app)
             (-active? app n))
      (observe-state app iref o n))))

