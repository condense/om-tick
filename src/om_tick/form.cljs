(ns om-tick.form
  (:require-macros [condense.macros :refer [inspect console-timer]])
  (:require [om-tick.field :refer [field? reset-field initialise-field validate-field
                                   field-zipper field-reduce field-edit]]
            [om-tick.app :as app]))


(defn fmap [f m]
  (into (empty m) (for [[k v] m] [k (f v)])))


(defn form? [m]
  (contains? m :fields))


(defn reset-form [{:keys [fields] :as form}]
  (assoc form :fields (field-edit (field-zipper fields) reset-field)))


(defn load-data
  ([{:keys [data] :as form}]
    (load-data form data))
  ([form data]
   (reduce
     (fn [form-acc [k v]]
       (assoc-in form-acc [:fields k :value] v))
     form data)))


(defn extract-data
  "Extract current values from form as a map"
  [{:keys [fields]}]
  (into {} (->> fields (fmap :value))))


; TODO: Use field-zipper.
(defn clear-errors
  "Clear errors stored against field (from server etc)"
  [form]
  (let [field-keys (-> form :fields keys)]
    (reduce
      (fn [s field-key]
        (assoc-in s [:fields field-key :errors] []))
      (assoc form :non_field_errors [])
      field-keys)))


; TODO: Store non-field errors (:non_field_errors form)
(defn load-errors [form field-errors]
  (let [field-keys (keys field-errors)]
    (reduce
      (fn [s field-key]
        (assoc-in s [:fields field-key :errors]
                  (get field-errors field-key [])))
      (clear-errors form) field-keys)))


(defn initialise-form [form]
  {:pre [(form? form)]}
  (-> form
      (update :fields #(fmap initialise-field %))
      load-data))


(defn validate-fields [{:keys [fields]}]
  (field-edit (field-zipper fields) validate-field))


(defn form-logic [form]
  (-> form validate-fields))


(defn is-valid? [{:keys [fields non_field_errors]}]
  (field-reduce (field-zipper fields)
                (fn [acc {:keys [errors]}]
                  (and acc (empty? errors)))
                true)
  (and (empty? non_field_errors)
       (field-reduce (field-zipper fields)
                     (fn [acc {:keys [errors]}] (and acc (empty? errors)))
                     true)))


(defn form-app
  "
    The form app wrapps the basic form functionality up for reuse.

    * Only active if state map contains key k
    * Initialises form from payload
    * Apply an init-fn to payload if present
    * Apply standard form logic to derived state
    * Apply additional logic-fn if present
  "

  [k & {:keys [init-fn pre-init-fn init-logic? logic-fn form-logic?]
                     :or {init-logic? true
                          form-logic? true}}]
  (reify

    app/IAppActive
    (app-active? [_ state]
      (contains? state k))

    app/IInitState
    (init-state [_ payload]
      (cond-> payload
        (ifn? pre-init-fn) (update-in [k] pre-init-fn)
        init-logic? (update-in [k] initialise-form)
        (ifn? init-fn) (update-in [k] init-fn)))

    app/IDeriveState
    (derive-state [_ state]
      (cond-> state
        form-logic? (update-in [k] form-logic)
        (ifn? logic-fn) (update-in [k] logic-fn)))))